# FabAccess Setup - Step By Step

This document provides a step by step Instruction on how to get FabAcess running. At the end of this description you will have:
- 1 or more Shellies registered to you system
- 1 or more users registered to your system
- QR-Codes generated to acess a machine
- 1 Shelly configured as a door-opener
- 1 Shelly configured to identify if a machine is just switched on or realy running (TO-DO)


**Step 1 Installing the BFFH-Server**

there are multiple ways to install the BFFH server. This can bei either done via 
- docker - see docker installation document
- installing from source - see installing from source documentation
- installing on Ubuntu for dummies

**Step 2 Installing the FabAccess App**

get the App via Apple Store or Google Apps.

**Step 3 Connect the App and the Server**

First you need to find the IP of the server. This can be done by typing
`ip a`
on the console of the system where the BFFH-Server is running. Use the adress listed under BROADCAST.

Start the server. If you are using the docker, this is done by using <br> 
`docker-compose up -d`. <br> 
If you compiled the server on your system this is done by entering <br>
`./diflouroborane -c examples/bffh.dhall --load examples` <br>
and then <br> 
`./diflouroborane -c examples/bffh.dhall`. <br>
You will see some debug information, with probably some warnings.

Open the App. You will be asked to connect to a Host. Tap "DEMO HOST ADRESS" and change the IP to the IP of your Server, do not change the port number (everything after the IP. This should look like `192.168.1.15:59661`).
Tap "SELECT HOST".

You will be asked to sign in. For Version 0.2 only the Option "LOGIN WITH PASSWORD" ist available. Use `Testuser` and the passwort `secret` to log in.

You will find an overview of the installed machines including the option "SCAN QR-CODE".
Next step is setting up you machines so they can be switched on an off.

**Step 4 Prepare your Shellies**

as long as your Shelly has not been given the credentials for a WLAN, it will create an access point (AP) for configuration when connected to the supply voltage. This AP will appear in your list of WLAN. 
Connect to this Shelly-AP and connect to `192.168.33.1` in your browser. A configuration page should appear.
If your Shelly is already connected to your WLAN, you must find the assigned IP-Adress (e.g. by looking into your router). Enter this IP Adress in your browser and you will get the configuration page.

**Shelly MQTT Client setup**

goto "Internet & Security" -> "Advanced - Developer Settings"
enable "MQTT"
enter the IP-Adress from your Server in the field "IP-Adress"
As we did not define MQTT credentials in mosquitto yet, no creadentials need to be filled in.
To find the "ID" of your Shelly activate "Use custom MQTT prefix" (but do not change it!). This should be somthing like:
`shelly1-123456789ABC` for a Shelly 1
`shelly1pm-123456` for a Shelly 1PM
note this ID for later
**- save**
**- re-check the settings!**

**Shelly WLAN Client setup**

goto "Internet & Security" -> "WIFI MODE - CLIENT"
Set WLAN Credentials

**Adding a Shelly to your server**
To understand the underlaying concept of actors and machines, please see the "configuration part" of the documentation. Four our example we will assume we have one actor (shelly) per machine.

**Tip**
Prior to modifying the configuration files the proper working of the MQTT broker should be tested. To test the broker it is the best to use a second (linux) computer with a different IP adress. To test if the broker allows access from an external IP address open a MQTT subscriber on the second computer by typing <br>
`mosquitto_sub -h 192.168.1.15 -t /test/topic` (change the IP adress to the adress of your server).<br>
Use<br> 
`mosquitto_pub -h localhost -t /test/topic -m "Hallo from BFFH-Server!"`<br> 
to send a message to the other computer. If the message appears, everything is ok. When not, this should be first solved, as a connection to the shellies will not be possible this way.<br>
If you are interested in communication between the shellies and the BFFH-Server you can use<br>
`mosquitto_sub -h 192.168.1.15 -t shellies/#` <br>
(change the IP adress to your needs). You will see some values popping op from time to time.

**Configure Diflouroborane**
Open the file "bffh.dhall" in the GUI Editor (just by double-clicking it) or use `nano bffh.dhall` in your console.<br>

Link the server to the MQTT-broker<br>
find the line which starts with `, listens`. You will find three lines stating addresses. The third address needs to be changed to the adress of your MQTT broker (most likely the IP adress of your BFFH server)

First you have to make your "actors" (in our case the Shellies) know to the system.<br>
Go to the line where it starts with `, actors =` and after the `{` you can enter your Shelly with <br>
`shelly1-123456789ABC = { module = "Shelly", params = {=}}`<br>
The ID of the Shelly should match the ID of your Shelly. Here you can enter as many actors as you want, each separated by a `,`.

Now you have to link a "machine" to an "actor".<br>
Go to the line starting with `{actors_connections =` and after the following `[` you add<br>
`{ machine = "Identifier-of-your-Machine", actor = "shelly1-E8DB84A1CFF4" }` <br>
using your own Name-of-your-Machine and the Shelly-ID of the related actor.

Now you have to set the "access-permissions" to your "machine".<br>
Go to the line starting with `, machines =`. and after the `{` you can add a machine:<br>
`Identifier-of-your-Machine =` <br>
`   { description = Some "I am your first Testmachine"`<br>
`   , disclose = "lab.test.read"`<br>
`   , manage = "lab.test.admin"`<br>
`    , name = "Name of the Machine"`<br>
`    , read = "lab.test.read"`<br>
`    , write = "lab.test.write"`<br>
`    },`<br>

Please be aware that "Identifier-of-your-Machine" is the internal ID for BFFH. The name of the machine shown in the App will be "Name of the Machine".<br> 
The given permissions are ok to start with (if you did not change the roles of the Testuser). To find out more about the permission concept see the "configuration" part of the documentation.

**- save** (if you are using nano, this will be Ctrl-O )

**-restart the BFFH-server**
**Important**
every time you change the bffh.dhal you need to reload the settings (otherwise the App will not connect to the server on restart): 
`./diflouroborane -c examples/bffh.dhall --load examples`
and restart start Diflouroborane:
`./diflouroborane -c examples/bffh.dhall`

Open the App, an you should see the newly created machine in the list. By tapping "USE" you will activate the machine (Shelly will click, the MQTT-listener should promp an "on"), by tapping "GIVEBACK" you will deactivat the machine.

**Creating a QR-Code for your machine**
A QR code allows users to directly enter the UI of the machine, where the machine can be used or given back. The QR code should contain the following content:<br>
`urn:fabaccess:resource:{MachineID}`<br>
e.g.<br>
`urn:fabaccess:resource:Identifyer-of-your-Machine`<br>

QR-Codes can be generated on various pages in the internet (e.g. https://www.qrcode-generator.de), the "Type" of the QR code should be "Text". The generated code can be directly scanned by the FabAccess App in the machine overview.

**Adding a user**
Adding a user to the system consists of two steps
- creating the user
- provide permissions

Users are defined in the file users.toml. To add a user simply add<br>
`[Name-of-the-User]`<br>
`roles = ["Name-of-a-role/internal", "Name-of-another-role/internal"]`<br>
`priority = 0`<br>
`passwd = "the-chosen-password"`<br>
`noot = "whatever-this-means"`<br>
Adding users or changing existing users does NOT require to restart the system (tested?) 

The permissions of the user are defined by the linked roles. The roles are defined in the file bffh.dhall.
Open the file bffh.dhall an find the line starting with `, roles =`<br>
The concept of the role management is described in the "configuration" part of the documentation.
To keep it simple we create a role called "ChainsawUser"
`ChainsawUser =`<br>
`{ permissions = `<br>
`[ "lab.machines.chainsaw.write"` - allows the user to use the machine<br>
`, "lab.machines.chainsaw.read"`- allows the user to read see the status of the machine<br>
`, "lab.machines.chainsaw.disclose"` - allows the user to see the machine in the machine overview<br>
`]`

If a user assinged to this role uses the chainsaw, no other user is able to use it until this user gives the chainsaw back. To unlock the machine from the user, admin permissions are needed. So there could be an admin role like
`ChainsawAdmin =`<br>
`{ parents = ["ChainsawUser"]`<br> - inherits all the permissions of the ChainsawUser
`, permissions = ["lab.machines.chainsaw.admin"]`<br> - addinional admin permissions
`}`

The `machine` should be defined as:
`Identifier-of-your-Chainsaw =` <br>
`   { description = Some "Beware of Freddy!"`<br>
`   , disclose = "lab.machine.chainsaw.disclose"`<br>
`   , manage = "lab.machine.chainsaw.admin"`<br>
`    , name = "Chainsaw"`<br>
`    , read = "lab.machine.chainsaw.read"`<br>
`    , write = "lab.machine.chainsaw.write"`<br>
`    },`<br>

If a user is asigned to "ChainsawUser/internal" he/she will be able to see and used the chainsaw in FabAccess.

**Using a Shelly as a door opener (electronic wise)**
In version 0.2 a door opener functionality is not implemented. The specific behaviour of a door opener is, to activate a door openeing relais only for a few seconds. This behaviour is not yet implemented in FabAccess, but there is decent way to implement it by other means.
The simple Shellies (1, 1pm, 2.5) have an internal timer "AUTO-OFF" which can be set. To use this timer you have to access the settings of the Shelly via a browser on your computer. To do so, you have to know the IP adress your Shelly is assinged to. This can normally found out in the router of your Wifi. By entering this IP adress in your browser you will access the main menu of your Shelly.

Go to "Timer" and set the "AUTO-OFF" to e.g. 3 seconds.<br>
Define a machine called "door" in the bffh.dhall<br>
- define the actor:<br>
`shelly1-123456789ABC = { module = "Shelly", params = {=}}`<br>
- define the machine:<br>
`{ machine = "door", actor = "shelly1-123456789ABC" }` <br>
- set permissions for the machine:<br>
`door =` <br>
`   { description = Some "close it firmly"`<br>
`   , disclose = "lab.door.disclose"`<br>
`   , manage = "lab.door.admin"`<br>
`    , name = "Door to the Lab"`<br>
`    , read = "lab.door.read"`<br>
`    , write = "lab.door.write"`<br>
`    },`<br>
- create a role having ALL permissions to the door<br>
`DoorUser =`<br>
`{ permissions = `<br>
`[ "lab.door.write"` - allows the user to use the door<br>
`, "lab.door.read"`- allows the user to read see the status of the door<br>
`, "lab.door.disclose"` - allows the user to see the machine in the machine overview<br>
`, "lab.door.admin"`<br>
`]`<br>
- assign the role DoorUser/internal to all users

It is imporatant all users have admin aka manage permissions, as the request to open the door by a user, thet the door "in Use" by this user. The door can only be re-activated when the previous user "un-uses" the door or if an othe user can "force free" the door prior to using the door hin/herself.<br>
**Note** in this special case, where all users will need admin capabilities the role could also contain only the permission `lab.door.use` and all permissions (disclos, manage, read, write) assigned to the machine would simply match `lab.door.use` (e.g. disclose = "lab.door.use"`).

**Identify if a machine is just switched on or realy running (TO-DO)
