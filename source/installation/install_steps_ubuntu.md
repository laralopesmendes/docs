# Install on Ubuntu for "Dummies"

This description is how to compile and set up Diflouroborane on Ubuntu 20.04.3 LTS clean install. Other releases or distros might work as well.
The process is quite lengthy but at the end you will have a FabAccess running to you needs.
... as I said: for complete dummies, if someone finds a better solution, please post suggestions on: 
https://fabaccess.zulipchat.com/#narrow/stream/255963-General/topic/Demo

1. Get your system up-to-date 
`sudo apt-get update && sudo apt-get upgrade`

2. Get rustup 
`sudo apt install curl`
`curl --proto 'https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
**restart the console**

3. Get capnproto, gsasl and git
`sudo apt-get install gsasl`
`sudo apt-get install capnproto`
`sudo apt install git`

4. Create a target directory for BFFH
there might be better places compared to where I created it, but it works...
`mkdir BFFH`
`cd BFFH`

5. Clone the Diflouroborane repository
`git clone https://gitlab.com/fabinfra/fabaccess/bffh --recursive --branch main`

6. For compiling some dependencies were missing on Ubuntu
`git submodule update --init`
`sudo apt install libgsasl7-dev`
`sudo apt install cmake`
`sudo apt install libclang-dev`
`sudo apt install libssl-dev`

7. Open the subdirectory and start compiling
`cd bffh`
`cargo build --release`
**if the compiler prompts somthing like "error: linker 'cc' not found":**
`sudo apt install build-essential`
`cargo build --release`
 

8. Copy the configuration files (best done with the GUI filemanager of Ubuntu)
copy files from "bffh/examples"
paste them into "bffh/target/release/examples"

9. Install mosquitto MQTT broker
Diflouroborane uses mosquitto MQTT broker to communicate with the Shellies. Starting from Ubuntu version 18.04, Mosquitto is already inside the official repositories.
`sudo apt update -y && sudo apt install mosquitto mosquitto-clients -y`


10. Configuring mosquitto broker
for some reason, starting with version 2.x mosquitto does not allow external communication via the broker per default. This needs to be changed via a config file:
11. Stop mosquitto
`sudo service mosquitto stop`
12. Change into the "etc/mosquitto/" directory (lots of `cd ..` then `cd etc/mosquitto`)
13. Edit the configuration fil:
`sudo nano  mosquitto.conf`
add:
`listener 1883` 
`allow_anonymous true`
Save (Strg-O) and close (Strg-X)
14. Enable mosquitto to start at each start of the system
`sudo systemctl enable mosquitto`
15. Restart the system.

The BFFH-server can be found at the /target/release folder. Prior to starting the system you need to copy the files from `bffh/examples` to `bffh/target/release/examples` This ist best done by using the GUI filemanager.

To get at least minimum functionality the bffh.dhall should be modified. The lines:

 `-- , init_connections = [] : List { machine : Text, initiator : Text }`
 ` , init_connections = [{ machine = "Testmachine", initiator = "Initiator" }]`
 ` , initiators = --{=}`
 ` { Initiator = { module = "Dummy", params = { uid = "Testuser" } } }`

should be modified to:

 `, init_connections = [] : List { machine : Text, initiator : Text }`
 ` --, init_connections = [{ machine = "Testmachine", initiator = "Initiator" }]`
 ` , initiators = {=}`
 ` --{ Initiator = { module = "Dummy", params = { uid = "Testuser" } } }`

To start the server change into the directory by using `cd target/release` and using:

`./diflouroborane -c examples/bffh.dhall --load examples`
an then:

`./diflouroborane -c examples/bffh.dhall`

**BUT** prior to starting bffh, you should first configure the bffh.dhall file (see the "Use FabAcess" section).



