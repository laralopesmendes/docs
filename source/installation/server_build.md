# Build Server from Source

1. Install [rustup.rs](rustup.rs). Distribution packages for rustc are generally way too old
1. `$ rustup install stable`
1. Get yourself a directory to clone BFFH into
    * If you put this dir on a SSD you can speed up build times by 5-10 times.
1. `git clone --recursive --branch stable`
    * ... stable ... TODO...
    * You can also check out the `development` branch but keep in mind that until Beta it has no stability guarantee. It may work. It may make you a sandwich. But it may also set your hat on fire and fill your shoes with orange juice. *You have been warned*.
1. `cargo install --path .`
    * if you add `--debug` you get a debug build. It gives you much more logging output but it's slower to run and is almost spammy
1. Make yourself a coffee. Or tea. Or open $beverage of your choice. You earned it! (And you'll be looking at "**Compiling** <stuff>" for a while.)
    * If you get `error: failed to run custom build command for 'gsasl-sys v0.2.3'` or something like that with the stderr output reading "[…]Unable to find libclang[…]":
        * `export LIBCLANG_PATH=/usr/lib`
           Or wherever `libclang.so` is installed on your computer. It's usually `/usr/lib/libclang.so` or `/usr/lib/llvm/12/lib/libclang.so` or similar. If you can't find it, consult your package manager
    * If you get any other error open an issue